-- 14 Общее число заказов, среднее количество товаров в заказе, суммарную стоимость всех покупок,
-- среднюю / минимальную / максимальную стоимость заказа
SELECT user_id,
       round(avg(array_length(product_ids, 1)), 2) avg_order_size,
       count(distinct order_id) orders_count,
       sum(order_price) sum_order_value,
       round(avg(order_price), 2) avg_order_value,
       min(order_price) min_order_value,
       max(order_price) max_order_value
FROM   user_actions
    LEFT JOIN orders using(order_id) join (SELECT order_id,
                                                  sum(price) as order_price
                                           FROM   (SELECT order_id,
                                                          unnest(product_ids) as product_id
                                                   FROM   orders) t1 join products using(product_id)
                                           GROUP BY order_id) t2 using(order_id)
WHERE  order_id not in (SELECT order_id
                        FROM   user_actions
                        WHERE  action like '%cancel%')
GROUP BY user_id
ORDER BY user_id limit 1000;



-- 16 10 самых популярных товаров в сентябре 2022 года.
SELECT count(distinct order_id) times_purchased,
       name
FROM   products join (SELECT DISTINCT product_id,
                                      order_id
                      FROM   (SELECT unnest(product_ids) as product_id,
                                     order_id
                              FROM   orders join courier_actions using(order_id)
                              WHERE  date(time) between '2022-09-01'
                                 and '2022-09-30'
                                 and action like '%deliver%') t1) t2 using(product_id)
GROUP BY product_id
ORDER BY 1 desc limit 10;



-- 17 Среднее значение доли отмененных товаров для мужчин, женщин и поля "неизвестно".
SELECT coalesce(sex, 'unknown') sex,
       round(avg(cancel_rate), 3) avg_cancel_rate
FROM   (SELECT user_id,
               round(count(distinct order_id) filter (WHERE action = 'cancel_order')::decimal / count(distinct order_id),
                     2) as cancel_rate,
               count(distinct order_id) as orders_count
        FROM   user_actions
        GROUP BY user_id) t LEFT join users using(user_id)
GROUP BY sex
ORDER BY sex;


-- 20 Поиск id и расчет возраста пользователя и курьера самого большого заказа.
SELECT DISTINCT order_id,
                user_id,
                courier_id,
                date_part('year', age((SELECT max(time) FROM user_actions), 
                                      c.birth_date))::int courier_age, 
                date_part('year', age((SELECT max(time) FROM user_actions), 
                                      u.birth_date))::int user_age
FROM   orders   join courier_actions using(order_id) 
				join couriers c using(courier_id) 
                join user_actions using(order_id) 
                join users u using(user_id)
WHERE  array_length(product_ids, 1) = (SELECT max(array_length(product_ids, 1))
                                       FROM   orders);
   
   
-- 21 SELF JOIN, Расчет частоты встречаемоти товаров в заказах пользователей.
with cte as (SELECT order_id,
                    product_id,
                    name
             FROM   (SELECT order_id,
                            unnest(product_ids) as product_id
                     FROM   orders
                     WHERE  order_id not in (SELECT order_id
                                             FROM   user_actions
                                             WHERE  action like '%cancel%')) ord
                 LEFT JOIN products using(product_id))
                 
SELECT count(distinct t.order_id) as count_pair,
       array[t.name, tt.name] as pair
FROM   cte t cross join cte tt
WHERE  t.order_id = tt.order_id
   and t.name < tt.name
GROUP BY pair
ORDER BY count_pair desc, pair;