-- 4 Расчет накопительной суммы заказов
SELECT date,
       orders_count,
       (sum(orders_count) OVER (ORDER BY date))::integer orders_cum_count
FROM   (SELECT date(creation_time) date,
               count(order_id) orders_count
        FROM   orders
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action like '%cancel%')
        GROUP BY date) t;
 
 
-- 5 Расчет порядкового номера каждого заказа для каждого пользователя в таблице user_actions.
SELECT user_id,
       order_id,
       time,
       row_number() OVER (PARTITION BY user_id
                          ORDER BY order_id) as order_number
FROM   user_actions
WHERE  order_id not in (SELECT order_id
                        FROM   user_actions
                        WHERE  action like '%cancel%')
ORDER BY user_id, order_id limit 1000;


-- 6 Расчет количества времени между заказами каждого пользователя.
SELECT user_id,
       order_id,
       time,
       row_number() OVER (PARTITION BY user_id
                          ORDER BY order_id) as order_number,
       lag(time) OVER (PARTITION BY user_id) as time_lag,
       time - lag(time) OVER (PARTITION BY user_id) as time_diff
FROM   user_actions
WHERE  order_id not in (SELECT order_id
                        FROM   user_actions
                        WHERE  action like '%cancel%')
ORDER BY user_id, order_id limit 1000;


-- 7 Расчет среднего времени между заказами пользователя.
SELECT user_id,
       (sum(time_diff) / (min(cnt)-1))::integer as hours_between_orders
FROM   (SELECT user_id,
               order_id,
               time,
               (extract(epoch
        FROM   time - lag(time)
        OVER (
        PARTITION BY user_id
        ORDER BY order_id)) / 3600) as time_diff, count(order_id)
        OVER (
        PARTITION BY user_id) cnt
        FROM   user_actions
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action like '%cancel%')) t
WHERE  cnt > 1
GROUP BY user_id
ORDER BY user_id limit 1000;


-- 8 Расчет скользящего среднего числа заказов за 3 предыдущих дня
SELECT date,
       orders_count,
       round(avg(orders_count) OVER (rows between 3 preceding and 1 preceding),
             2) as moving_avg
FROM   (SELECT date(creation_time) date,
               count(order_id) as orders_count
        FROM   orders
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action like '%cancel%')
        GROUP BY date) t
ORDER BY date;


-- 9 Курьеры, которые доставили в сентябре 2022 года заказов больше, чем в среднем все курьеры.
SELECT courier_id,
       delivered_orders,
       avg_delivered_orders,
       case when delivered_orders > (avg(avg_delivered_orders) OVER (PARTITION BY courier_id)) then true
            else false end::integer as is_above_avg
FROM   (SELECT courier_id,
               count(order_id) as delivered_orders,
               round(avg(count(order_id)) OVER (), 2) as avg_delivered_orders
        FROM   courier_actions
        WHERE  date_part('year', time) = 2022
           and date_part('month', time) = 9
           and action like '%deliver%'
        GROUP BY courier_id) t
ORDER BY courier_id