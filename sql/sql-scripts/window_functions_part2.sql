-- 10 Расчет числа первых и повторных заказов на каждую дату.
SELECT time::date as date,
       order_type,
       count(order_id) as orders_count
FROM   (SELECT user_id,
               order_id,
               time,
               case when time = min(time) OVER (PARTITION BY user_id) then 'Первый'
                    else 'Повторный' end as order_type
        FROM   user_actions
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action = 'cancel_order')) t
GROUP BY date, order_type
ORDER BY date, order_type;



-- 11 Расчет числа и доли первых и повторных заказов на каждую дату.
SELECT date,
       order_type,
       count(order_id) as orders_count,
       round(count(order_id) / sum(count(order_id)) OVER (PARTITION BY date), 2) as orders_share
FROM   (SELECT order_id,
               date(time) as date,
               case when rank() OVER (PARTITION BY user_id
                                      ORDER BY time) = 1 then 'Первый'
                    else 'Повторный' end as order_type
        FROM   user_actions
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action like '%cancel%')) t
GROUP BY date, order_type
ORDER BY date, order_type;



-- 12 Средняя цена товаров с учетом и без учета самого дорогого.
SELECT product_id,
       name,
       price,
       round(avg(price) OVER (), 2) as avg_price,
       round(avg(price) filter (WHERE price <> (SELECT max(price)
                                         FROM   products))
OVER (), 2) as avg_price_filtered
FROM   products
ORDER BY price desc, product_id;



-- 13 Количество созданных и отмененных заказов пользователем на каждый момент времени, доля отмененных заказов.
SELECT user_id,
	   order_id,
       action,
       time,
       created_orders,
       canceled_orders,
       round(canceled_orders::decimal / created_orders, 2) as cancel_rate
FROM   (SELECT user_id,
               order_id,
               action,
               time,
               count(order_id) filter (WHERE action like '%create%') OVER (PARTITION BY user_id
                                                                           ORDER BY time) created_orders,
               count(order_id) filter (WHERE action like '%cancel%') OVER (PARTITION BY user_id
                                                                           ORDER BY time) canceled_orders
        FROM   user_actions
        ORDER BY user_id, order_id, time) t limit 1000;
        

-- 14 Топ 10% курьеров по количеству доставленных за всё время заказов.
with cte as (SELECT courier_id,
                    orders_count,
                    row_number() OVER (ORDER BY orders_count desc, courier_id) as courier_rank
             FROM   (SELECT courier_id,
                            count(order_id) as orders_count
                     FROM   courier_actions
                     WHERE  action like '%deliver%'
                     GROUP BY courier_id) t)
SELECT  courier_id,
		orders_count,
        courier_rank
FROM   cte
WHERE  (courier_rank::decimal / (SELECT max(courier_rank)
                                 FROM   cte)) <= 0.1005
ORDER BY courier_rank;



-- 15 Курьеры, которые работают 10 и более дней, количество доставленных ими заказов.
SELECT  courier_id,
		delivered_orders,
        days_employed
FROM   (SELECT courier_id,
               delivered_orders,
               date_part('days', max(maxt) OVER () - mint)::integer as days_employed
        FROM   (SELECT courier_id,
                       max(time) as maxt,
                       min(time) as mint,
                       count(order_id) filter (WHERE action like '%deliver%') as delivered_orders
                FROM   courier_actions
                GROUP BY courier_id) t) t2
WHERE  days_employed >= 10
ORDER BY days_employed desc, courier_id;



-- 16 Стоимость каждого заказа, ежедневная выручка сервиса и доля стоимости каждого заказа в ежедневной выручке, выраженная в процентах. 
SELECT order_id,
       creation_time,
       order_price,
       daily_revenue,
       round((min(order_price)::decimal / min(daily_revenue))*100,
             3) as percentage_of_daily_revenue
FROM   (SELECT creation_time,
               sum(price) OVER (PARTITION BY date(creation_time)) as daily_revenue,
               order_id,
               sum(price) OVER (PARTITION BY order_id) as order_price
        FROM   (SELECT creation_time,
                       order_id,
                       unnest(product_ids) as product_id
                FROM   orders) t join products using(product_id)
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action like '%cancel%')) t
GROUP BY order_id, creation_time, order_price, daily_revenue
ORDER BY date(creation_time) desc, percentage_of_daily_revenue desc, order_id;



-- 17 daily_revenue (дневная выручка), revenue_growth_abs (ежедневный прирост выручки), revenue_growth_percentage (ежедневный прирост выручки в процентах)
SELECT date,
       round(daily_revenue, 1) as daily_revenue,
       round(coalesce(daily_revenue - lag(daily_revenue, 1) OVER (ORDER BY date), 0),
             1) as revenue_growth_abs,
       round(coalesce(round((daily_revenue - lag(daily_revenue, 1) OVER (ORDER BY date))::decimal / lag(daily_revenue, 1) OVER (ORDER BY date) * 100, 2), 0),
             1) as revenue_growth_percentage
FROM   (SELECT date(creation_time) as date,
               sum(price) as daily_revenue
        FROM   (SELECT order_id,
                       creation_time,
                       product_ids,
                       unnest(product_ids) as product_id
                FROM   orders
                WHERE  order_id not in (SELECT order_id
                                        FROM   user_actions
                                        WHERE  action like '%cancel%')) t1
            LEFT JOIN products using(product_id)
        GROUP BY date) t2
ORDER BY date;
