-- 8 Расчет среднего значения количества заказов, отклонений от среднего значения заказов для каждого пользователя.
with cte as (SELECT user_id,
                    count(distinct order_id) as cnt
             FROM   user_actions
             GROUP BY user_id)
SELECT  user_id,
        count(distinct order_id) as orders_count,
        (SELECT round(avg(cnt), 2) FROM cte) as orders_avg, 
 		count(distinct order_id) - 
        	(SELECT round(avg(cnt), 2) FROM cte) as orders_diff
FROM 	user_actions
GROUP BY user_id
ORDER BY user_id limit 1000;



-- 9 Назначение скидок на товары большие или меньшие средней цены на 50.
SELECT  product_id,
        name,
        price,
        case 
       		when price > (SELECT avg(price) FROM   products) + 50 
            then round(price * 0.85, 2) 
            when price < (SELECT avg(price) FROM   products) - 50 
            then round(price * 0.90, 2) else price 
        end as new_price
FROM    products
ORDER BY price desc, product_id;



-- 13 Поиск недоставленных заказов, отмененных заказов, заказов в пути.
SELECT count(distinct order_id) as orders_undelivered,
       count(order_id) filter (WHERE action = 'cancel_order') as orders_canceled,
       count(distinct order_id) - count(order_id) filter (WHERE action = 'cancel_order') as orders_in_process
FROM   user_actions
WHERE  order_id in (SELECT order_id
                    FROM   courier_actions
                    WHERE  order_id not in (SELECT order_id
                                            FROM   courier_actions
                                            WHERE  action = 'deliver_order'));
                                            
                               
-- 17 Средний размер заказов, отмененных пользователями мужского пола.
SELECT round(avg(array_length(product_ids, 1)), 3) as avg_order_size
FROM   orders
WHERE  order_id in (SELECT order_id
                    FROM   user_actions
                    WHERE  action like '%cancel%'
                       and user_id in (SELECT user_id
                                    FROM   users
                                    WHERE  sex = 'male'));
                                    
  
  
-- 18 Расчет возраста пользователей с заполнением пропусков средним.
with age_cte as (SELECT user_id,
                          date_part('year', age((SELECT max(time)
                                          FROM   user_actions), birth_date)) as age
                   FROM   users)
SELECT user_id,
       coalesce(age, (SELECT round(avg(age))
               FROM   age_cte))::integer as age
FROM   age_cte
ORDER BY user_id;


-- 19 Расчет времени принятия, доставки заказа, время, затраченное на доставку.
SELECT order_id,
       min(time) as time_accepted,
       max(time) as time_delivered,
       (extract(epoch
FROM   max(time) - min(time))/60)::integer as delivery_time
FROM   courier_actions
WHERE  order_id in (SELECT order_id
                    FROM   orders
                    WHERE  array_length(product_ids, 1) > 5)
   and order_id not in (SELECT order_id
                     FROM   user_actions
                     WHERE  action = 'cancel_order')
GROUP BY order_id
ORDER BY order_id;



-- 22 Поиск 10 самых популярных товаров.
SELECT  product_id,
		times_purchased
FROM   (SELECT count(*) as times_purchased,
               unnest(product_ids) as product_id
        FROM   orders
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action like '%cancel%')
        GROUP BY product_id
        ORDER BY times_purchased desc limit 10) ss
ORDER BY product_id;



-- 23 Поиск заказов, которые содержат хотя бы 1 из 5 наиболее популярных товаров.
SELECT DISTINCT order_id,
                product_ids
FROM   (SELECT order_id,
               product_ids,
               unnest(product_ids) as product_id
        FROM   orders) t
WHERE  product_id in (SELECT product_id
                      FROM   products
                      ORDER BY price desc limit 5)
ORDER BY order_id;
