-- 2.2.5
-- Выручку, полученную в этот день. (revenue)
-- Выручку с заказов новых пользователей, полученную в этот день. (new_users_revenue)
-- Долю выручки с заказов новых пользователей в общей выручке, полученной за этот день. (new_users_revenue_share)
-- Долю выручки с заказов остальных пользователей в общей выручке, полученной за этот день. (old_users_revenue_share
SELECT date,
       revenue,
       new_users_revenue,
       round(new_users_revenue / revenue*100, 2) as new_users_revenue_share,
       round(100 - new_users_revenue / revenue*100, 2) as old_users_revenue_share
FROM   (SELECT date,
               sum(price) as revenue,
               sum(price) filter (WHERE date = date(min_time)) as new_users_revenue
        FROM   (SELECT date(creation_time),
                       order_id,
                       unnest(product_ids) as product_id
                FROM   orders join user_actions using(order_id)
                WHERE  order_id not in (SELECT order_id
                                        FROM   user_actions
                                        WHERE  action like '%cancel%')) t1 join products using(product_id) join (SELECT order_id,
                                                                                        min(time) OVER (PARTITION BY user_id) as min_time
                                                                                 FROM   user_actions) t2 using(order_id)
        GROUP BY date) tt1
ORDER BY date;


-- 2.2.6
-- Суммарную выручку, полученную от продажи этого товара за весь период. (revenue)
-- Долю выручки от продажи этого товара в общей выручке, полученной за весь период. (share_in_revenue)
-- Товары, округлённая доля которых в выручке составляет менее 0.5%, объединены в общую группу с названием «ДРУГОЕ».
SELECT product_name,
       sum(revenue) as revenue,
       sum(share_in_revenue) as share_in_revenue
FROM   (SELECT case when share_in_revenue < 0.5 then 'ДРУГОЕ'
                    else name end as product_name,
               revenue,
               share_in_revenue
        FROM   (SELECT name,
                       sum(price) as revenue,
                       round(sum(price) /sum(sum(price)) OVER ()*100, 2) as share_in_revenue
                FROM   (SELECT order_id,
                               unnest(product_ids) as product_id
                        FROM   orders join user_actions using(order_id)
                        WHERE  order_id not in (SELECT order_id
                                                FROM   user_actions
                                                WHERE  action like '%cancel%')) t1 join products using(product_id)
                GROUP BY product_id, name) tt1) ttt1
GROUP BY product_name
ORDER BY revenue desc;



-- 2.2.7
-- Выручку, полученную в этот день.(revenue)
-- Затраты, образовавшиеся в этот день. (costs)
-- Сумму НДС с продажи товаров в этот день. (tax)
-- Валовую прибыль в этот день (выручка за вычетом затрат и НДС). (gross_profit)
-- Суммарную выручку на текущий день. (total_revenue)
-- Суммарные затраты на текущий день. (total_costs)
-- Суммарный НДС на текущий день. (total_tax)
-- Суммарную валовую прибыль на текущий день. (total_gross_profit)
-- Долю валовой прибыли в выручке за этот день (долю п.4 в п.1). (gross_profit_ratio)
-- Долю суммарной валовой прибыли в суммарной выручке на текущий день (долю п.8 в п.5).  (total_gross_profit_ratio)

with product_list as (SELECT unnest(array['сахар', 'сухарики', 'сушки', 'семечки', 'масло льняное', 'виноград', 'масло оливковое', 'арбуз',
  											'батон', 'йогурт', 'сливки', 'гречка', 'овсянка', 'макароны', 'баранина', 'апельсины', 'бублики',
 											'хлеб', 'горох', 'сметана', 'рыба копченая', 'мука', 'шпроты', 'сосиски', 'свинина', 'рис',
  											'масло кунжутное', 'сгущенка', 'ананас', 'говядина', 'соль', 'рыба вяленая', 'масло подсолнечное',
  											'яблоки', 'груши', 'лепешка', 'молоко', 'курица', 'лаваш', 'вафли', 'мандарины']) 
                      									as product_name), 
       revenue_tax_cte as (SELECT date,
                                                           sum(revenue) as revenue,
                                                           sum(tax) as tax
                                                    FROM   (SELECT date(o.creation_time) as date,
                                                                   name,
                                                                   sum(price) as revenue,
                                                                   case when name = any(SELECT product_name
                                                                                 FROM   product_list) then sum(round(price * 0.1 / 1.1, 2)) else sum(round(price * 0.2 / 1.2, 2)) end as tax
                                                            FROM   (SELECT order_id,
                                                                           unnest(product_ids) as product_id
                                                                    FROM   orders join user_actions using(order_id)
                                                                    WHERE  order_id not in (SELECT order_id
                                                                                            FROM   user_actions
                                                                                            WHERE  action like '%cancel%')) t1 join products using(product_id) join orders o using(order_id)
                                                            GROUP BY date, name) tt1
                                                    GROUP BY date), 
       cost_cte as (SELECT date,
                                    case when month = 8 then round(number_bonus_couriers*400+orders_per_day_delivered*150+orders_per_day_accepted*140+120000,
                                                                   2)
                                         when month = 9 then round(number_bonus_couriers*500+orders_per_day_delivered*150+orders_per_day_accepted*115+150000,
                                                                   2) end as costs
                             FROM   (SELECT deliver_date as date,
                                            date_part('month', deliver_date)::int as month,
                                            count(distinct courier_id) filter (WHERE orders_per_courier > 4) as number_bonus_couriers,
                                            max(orders_per_day_accepted) as orders_per_day_accepted,
                                            max(orders_per_day_delivered) as orders_per_day_delivered
                                     FROM   (SELECT accept_date,
                                                    deliver_date,
                                                    courier_id,
                                                    count(order_id) OVER (PARTITION BY courier_id,
                                                                                       deliver_date) as orders_per_courier,
                                                    count(order_id) OVER (PARTITION BY accept_date) as orders_per_day_accepted,
                                                    count(order_id) OVER (PARTITION BY deliver_date) as orders_per_day_delivered,
                                                    order_id
                                             FROM   (SELECT min(date(time)) filter (WHERE action like '%accept%') as accept_date,
                                                            min(date(time)) filter (WHERE action like '%deliver%') as deliver_date,
                                                            courier_id,
                                                            order_id
                                                     FROM   courier_actions
                                                     WHERE  order_id not in (SELECT order_id
                                                                             FROM   user_actions
                                                                             WHERE  action = 'cancel_order')
                                                     GROUP BY order_id, courier_id) t1) t2
                                     GROUP BY date
                                     ORDER BY date) t3
                             GROUP BY date, month, number_bonus_couriers, orders_per_day_accepted, orders_per_day_delivered)
SELECT date,
       revenue,
       costs,
       tax,
       revenue-costs-tax as gross_profit,
       sum(revenue) OVER (ORDER BY date) as total_revenue,
       sum(costs) OVER (ORDER BY date) as total_costs,
       sum(tax) OVER (ORDER BY date) as total_tax,
       sum(revenue-costs-tax) OVER (ORDER BY date) as total_gross_profit,
       round((revenue-costs-tax)::decimal / revenue * 100, 2) as gross_profit_ratio,
       round((sum(revenue-costs-tax) OVER (ORDER BY date))::decimal / sum(revenue) OVER (ORDER BY date) * 100,
             2) as total_gross_profit_ratio
FROM   revenue_tax_cte join cost_cte using(date)
ORDER BY date;