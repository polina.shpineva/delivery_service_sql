-- 2.2.1
-- Выручку, полученную в этот день. (revenue)
-- Суммарную выручку на текущий день. (total_revenue)
-- Прирост выручки, полученной в этот день, относительно значения выручки за предыдущий день. (revenue_change)
SELECT date,
       sum(price) as revenue,
       sum(sum(price)) OVER (ORDER BY date) as total_revenue,
       round((sum(price) - lag(sum(price)) OVER(ORDER BY date))/ lag(sum(price)) OVER(ORDER BY date) * 100,
             2) as revenue_change
FROM   (SELECT date(creation_time),
               unnest(product_ids) as product_id
        FROM   orders
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action like '%cancel%')) t join products using(product_id)
GROUP BY date
ORDER BY date;



-- 2.2.2
-- Выручку на пользователя (ARPU) за текущий день.
-- Выручку на платящего пользователя (ARPPU) за текущий день.
-- Выручку с заказа, или средний чек (AOV) за текущий день.
SELECT date,
       round(sum(price) / min(users), 2) as arpu,
       round(sum(price) / min(paying_users), 2) as arppu,
       round(sum(price) / count(distinct order_id), 2) as aov
FROM   (SELECT count(distinct user_id) as users,
               count(distinct user_id) filter (WHERE order_id not in (SELECT order_id
                                                               FROM   user_actions
                                                               WHERE  action like '%cancel%')) as paying_users, date(time)
        FROM   user_actions
        GROUP BY date(time)) t2 join (SELECT date(creation_time),
                                     unnest(product_ids) as product_id,
                                     order_id
                              FROM   orders
                              WHERE  order_id not in (SELECT order_id
                                                      FROM   user_actions
                                                      WHERE  action like '%cancel%')) t using(date) join products using(product_id)
GROUP BY date
ORDER BY date;



-- 2.2.3
-- Накопленную выручку на пользователя (Running ARPU).
-- Накопленную выручку на платящего пользователя (Running ARPPU).
-- Накопленную выручку с заказа, или средний чек (Running AOV).
SELECT date,
       round(running_revenue / total_users, 2) as running_arpu,
       round(running_revenue / running_paying_users, 2) as running_arppu,
       round(running_revenue / running_orders, 2) as running_aov
FROM   (SELECT date,
               revenue,
               sum(revenue) OVER (ORDER BY date) as running_revenue,
               daily_orders,
               sum(daily_orders) OVER (ORDER BY date) as running_orders,
               new_users,
               sum(new_users) OVER (ORDER BY date) as total_users,
               new_paying_users,
               sum(new_paying_users) OVER (ORDER BY date) as running_paying_users
        FROM   (SELECT date,
               count(distinct order_id) as daily_orders,
               sum(price) as revenue
        FROM   (SELECT date(creation_time) as date,
                       order_id,
                       unnest(product_ids) as product_id
                FROM   orders
                WHERE  order_id not in (SELECT order_id
                                        FROM   user_actions
                                        WHERE  action = 'cancel_order')) as tt1
            LEFT JOIN products using (product_id)
        GROUP BY date) as t1 
join    (SELECT start_day as date,
               count(distinct user_id) as new_users
        FROM   (SELECT min(time):: date as start_day,
                       user_id
                FROM   user_actions
                GROUP BY user_id) as tt2
        GROUP BY date) as t2 using (date) 
join    (SELECT start_day as date,
               count(distinct user_id) as new_paying_users
        FROM   (SELECT min(time):: date as start_day,
                       user_id
                FROM   user_actions
                WHERE  order_id not in (SELECT order_id
                                        FROM   user_actions
                                        WHERE  action = 'cancel_order')
                GROUP BY user_id) as tt3
        GROUP BY date) as t3 using (date)) ttt1
ORDER BY date;




-- 2.2.4
-- Выручку на пользователя (ARPU) для каждого дня недели.
-- Выручку на платящего пользователя (ARPPU) для каждого дня недели.
-- Выручку на заказ (AOV) для каждого дня недели.
SELECT t2.weekday,
       t2.weekday_number,
       round(sum(price) / min(users), 2) as arpu,
       round(sum(price) / min(paying_users), 2) as arppu,
       round(sum(price) / count(distinct order_id), 2) as aov
FROM   (SELECT count(distinct user_id) as users,
               count(distinct user_id) filter (WHERE order_id not in (SELECT order_id
                                                               FROM   user_actions
                                                               WHERE  action like '%cancel%')) as paying_users, date_part('isodow', date(time)) as weekday_number, to_char(date(time), 'Day') as weekday
        FROM   user_actions
        WHERE  date(time) between '2022-08-26'
           and '2022-09-08'
        GROUP BY weekday_number, weekday) t2
    RIGHT JOIN (SELECT date_part('isodow', date(creation_time)) as weekday_number,
                       to_char(date(creation_time), 'Day') as weekday,
                       unnest(product_ids) as product_id,
                       order_id
                FROM   orders
                WHERE  order_id not in (SELECT order_id
                                        FROM   user_actions
                                        WHERE  action like '%cancel%')
                   and date(creation_time) between '2022-08-26'
                   and '2022-09-08') t using(weekday_number) join products using(product_id)
GROUP BY t2.weekday, t2.weekday_number
ORDER BY weekday_number;
