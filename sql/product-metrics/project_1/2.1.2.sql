SELECT date,
       new_users,
       new_couriers,
       total_users,
       total_couriers,
       new_users_change,
       new_couriers_change,
       round((total_users::decimal/lag(total_users) OVER ())*100-100,
             2) as total_users_growth,
       round((total_couriers::decimal/lag(total_couriers) OVER ())*100-100,
             2) as total_couriers_growth
FROM   (SELECT date,
               new_users,
               new_couriers,
               (sum(new_users) OVER (ORDER BY date))::integer as total_users,
               (sum(new_couriers) OVER (ORDER BY date))::integer as total_couriers,
               round((new_users::decimal / lag(new_users) OVER ())*100-100,
                     2) as new_users_change,
               round((new_couriers::decimal / lag(new_couriers) OVER ())*100-100,
                     2) as new_couriers_change
        FROM   (SELECT count(distinct user_id) as new_users,
                       date(min_time)
                FROM   (SELECT user_id,
                               min(time) as min_time
                        FROM   user_actions
                        GROUP BY user_id) t1
                GROUP BY date(min_time)) t3 full join (SELECT count(distinct courier_id) as new_couriers,
                                                      date(min_time)
                                               FROM   (SELECT courier_id,
                                                              min(time) as min_time
                                                       FROM   courier_actions
                                                       GROUP BY courier_id) t2
                                               GROUP BY date(min_time)) t4 using(date)) t5