SELECT date,
       orders,
       first_orders,
       new_users_orders,
       round(first_orders/orders::decimal*100, 2) first_orders_share,
       round(new_users_orders/orders::decimal*100, 2) new_users_orders_share
FROM   (SELECT time::date date,
               count(distinct order_id) orders
        FROM   user_actions
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action = 'cancel_order')
        GROUP BY 1) t1 join (SELECT time::date date,
                            count(order_number) first_orders
                     FROM   (SELECT user_id,
                                    time,
                                    order_id,
                                    row_number() OVER(PARTITION BY user_id
                                                      ORDER BY time) order_number
                             FROM   user_actions
                             WHERE  order_id not in (SELECT order_id
                                                     FROM   user_actions
                                                     WHERE  action = 'cancel_order')) t2
                     WHERE  order_number = 1
                     GROUP BY 1) t3 using(date) join (SELECT date,
                                        sum(coalesce(orders_count, 0))::integer new_users_orders
                                 FROM   (SELECT user_id,
                                                min(time::date) date
                                         FROM   user_actions
                                         GROUP BY user_id) t4
                                     LEFT JOIN (SELECT user_id,
                                                       time::date date,
                                                       count(distinct order_id) orders_count
                                                FROM   user_actions
                                                WHERE  order_id not in (SELECT order_id
                                                                        FROM   user_actions
                                                                        WHERE  action = 'cancel_order')
                                                GROUP BY 1, 2) t5 using(user_id, date)
                                 GROUP BY date) t6 using(date)