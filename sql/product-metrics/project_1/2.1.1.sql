SELECT date,
       new_users,
       new_couriers,
       (sum(new_users) OVER (ORDER BY date))::integer as total_users,
       (sum(new_couriers) OVER (ORDER BY date))::integer as total_couriers
FROM   (SELECT count(distinct user_id) as new_users,
               date(min_time)
        FROM   (SELECT user_id,
                       min(time) as min_time
                FROM   user_actions
                GROUP BY user_id) t1
        GROUP BY date(min_time)) t3 LEFT join (SELECT count(distinct courier_id) as new_couriers,
                                              date(min_time)
                                       FROM   (SELECT courier_id,
                                                      min(time) as min_time
                                               FROM   courier_actions
                                               GROUP BY courier_id) t2
                                       GROUP BY date(min_time)) t4 using(date)