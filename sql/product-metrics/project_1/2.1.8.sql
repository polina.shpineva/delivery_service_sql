SELECT hour,
       successful_orders,
       canceled_orders,
       round(canceled_orders::decimal / (canceled_orders + successful_orders),
             3) as cancel_rate
FROM   (SELECT date_part('hour', time)::int as hour,
               count(distinct order_id) filter (WHERE order_id not in (SELECT order_id
                                                                FROM   user_actions
                                                                WHERE  action = 'cancel_order')) as successful_orders
        FROM   user_actions
        GROUP BY hour) t1
    LEFT JOIN (SELECT count(order_id) as canceled_orders,
                      date_part('hour', creation_time)::int as hour
               FROM   orders
               WHERE  order_id in (SELECT order_id
                                   FROM   user_actions
                                   WHERE  action = 'cancel_order')
               GROUP BY hour) t2 using(hour)
ORDER BY hour