SELECT paying_users,
       active_couriers,
       date,
       round((paying_users::decimal / (total_users))*100, 2) as paying_users_share,
       round((active_couriers::decimal / (total_couriers))*100, 2) as active_couriers_share
FROM   (SELECT date(time),
               count(distinct user_id) filter(WHERE order_id not in (SELECT order_id
                                                              FROM   user_actions
                                                              WHERE  action like '%cancel%')) as paying_users
        FROM   user_actions
        GROUP BY date) t1 join (SELECT date(time),
                               count(distinct courier_id) filter (WHERE order_id not in (SELECT order_id
                                                                                  FROM   user_actions
                                                                                  WHERE  action like '%cancel%')) as active_couriers
                        FROM   courier_actions
                        GROUP BY date) t2 using(date) join (SELECT date,
                                           count(distinct user_id) filter (WHERE date = min_date_user) as new_users,
                                           count(distinct courier_id) filter (WHERE date = min_date_courier) as new_couriers,
                                           (sum(count(distinct user_id) filter (WHERE date = min_date_user)) OVER (ORDER BY date))::integer as total_users,
                                           (sum(count(distinct courier_id) filter (WHERE date = min_date_courier)) OVER (ORDER BY date))::integer as total_couriers
                                    FROM   (SELECT user_id,
                                                   courier_id,
                                                   date(u.time) as date,
                                                   min(date(u.time)) OVER (PARTITION BY user_id) as min_date_user,
                                                   min(date(u.time)) OVER (PARTITION BY courier_id) as min_date_courier
                                            FROM   user_actions u full join courier_actions a using(order_id)) p1
                                    GROUP BY date) p using(date)
ORDER BY date