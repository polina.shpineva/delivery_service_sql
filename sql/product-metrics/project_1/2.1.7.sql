SELECT date,
       avg(minutes_to_deliver)::int as minutes_to_deliver
FROM   (SELECT order_id,
               date(time),
               (extract(epoch
        FROM   max(time) filter (
        WHERE  action like '%deliver%') - min(time) filter (
        WHERE  action like '%accept%')) / 60)::int as minutes_to_deliver
        FROM   courier_actions
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action = 'cancel_order')
        GROUP BY order_id, date) t1
GROUP BY date
ORDER BY date