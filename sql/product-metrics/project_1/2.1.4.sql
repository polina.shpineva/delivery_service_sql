SELECT date,
       single_order_users_share,
       100-single_order_users_share as several_orders_users_share
FROM   (SELECT round(((count(distinct user_id) filter (WHERE orders_count = 1)*100)::decimal /min(new_users)),
                     2) as single_order_users_share,
               date
        FROM   (SELECT count(distinct order_id) as orders_count,
                       date(time),
                       user_id
                FROM   user_actions
                WHERE  order_id not in (SELECT order_id
                                        FROM   user_actions
                                        WHERE  action like '%cancel%')
                GROUP BY date, user_id) t1 join (SELECT count(distinct user_id) as new_users,
                                                date
                                         FROM   (SELECT min(date(time)),
                                                        user_id,
                                                        date(time)
                                                 FROM   user_actions
                                                 WHERE  order_id not in (SELECT order_id
                                                                         FROM   user_actions
                                                                         WHERE  action like '%cancel%')
                                                 GROUP BY user_id, date) t2
                                         GROUP BY date) t3 using(date)
        GROUP BY date) t4
ORDER BY date