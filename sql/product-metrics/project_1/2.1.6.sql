SELECT date,
       round(paying_users::decimal / couriers_num, 2) as users_per_courier,
       round(orders_num::decimal / couriers_num, 2) as orders_per_courier
FROM   (SELECT date(time),
               count(distinct user_id) as paying_users
        FROM   user_actions
        WHERE  order_id not in (SELECT order_id
                                FROM   user_actions
                                WHERE  action = 'cancel_order')
        GROUP BY date) t1 join (SELECT count(distinct order_id) filter (WHERE action like '%accept%') as orders_num,
                               count(distinct courier_id) as couriers_num,
                               date(time)
                        FROM   courier_actions
                        WHERE  order_id not in (SELECT order_id
                                                FROM   user_actions
                                                WHERE  action = 'cancel_order')
                        GROUP BY date) t2 using(date)
ORDER BY date