### <ins>Проект 1.</ins> 
Разработка SQL-скриптов для расчета базовых <ins>продуктовых метрик</ins>, последующее создание визуализаций на основе полученных сводных таблиц и общего дашборда в инструменте Redash для сервиса доставки продуктов.

[ссылка на дашборд в Redash](http://redash.public.karpov.courses/public/dashboards/Ap69ZakoliuVQD7zjYSyDjZ4ErmZj0LqWeuyJdFt?org_slug=default) 

Скрипты расчета метрик можно посмотреть по [ссылке](../sql/product-metrics/project_1) : 

[../sql/product-metrics/project_1](../sql/product-metrics/project_1)

![dashboard_screen_1](../sql/product-metrics/project_1/img_1.png)
![dashboard_screen_2](../sql/product-metrics/project_1/img_2.png)


### <ins>Проект 2.</ins> 
Разработка SQL-скриптов для расчета <ins>экономических метрик</ins> продукта, создание визуализаций на основе полученных сводных таблиц и общего дашборда в инструменте Redash для сервиса доставки продуктов.

[ссылка на дашборд в Redash](http://redash.public.karpov.courses/public/dashboards/9OdhmodCfz4abse8gTVT6heXVSOCJ94mTPpwpMNf?org_slug=default)

Скрипты расчета метрик можно посмотреть по [ссылке](../sql/product-metrics/project_2) : 

[../sql/product-metrics/project_2](../sql/product-metrics/project_2)

![dashboard_screen_1](../sql/product-metrics/project_2/img_2_1.png)
![dashboard_screen_2](../sql/product-metrics/project_2/img_2_2.png)